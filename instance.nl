g3 1 1 0	# problem unknown
 14 11 1 4 7 	# vars, constraints, objectives, ranges, eqns
 3 1 0 0 0 0	# nonlinear constrs, objs; ccons: lin, nonlin, nd, nzlb
 0 0	# network constraints: nonlinear, linear
 12 14 12 	# nonlinear vars in constraints, objectives, both
 0 0 0 1	# linear network variables; functions; arith, flags
 0 0 12 0 2 	# discrete variables: binary, integer, nonlinear (b,c,o)
 31 14 	# nonzeros in Jacobian, obj. gradient
 0 0	# max name lengths: constraints, variables
 0 0 0 0 0	# common exprs: b,c,o,c1,o1
C0
o0
o2
n-1
o2
o0
v0
n-1
o0
o2
n-1
v6
n1
o2
n-1
o2
o0
v1
n-1
o0
o2
n-1
v7
n1
C1
o0
o2
n-1
o2
o0
v2
n-1
o0
o2
n-1
v8
n1
o2
n-1
o2
o0
v3
n-1
o0
o2
n-1
v9
n1
C2
o0
o2
n-1
o2
o0
v4
n-1
o0
o2
n-1
v10
n1
o2
n-1
o2
o0
v5
n-1
o0
o2
n-1
v11
n1
C3
n0
C4
n0
C5
n0
C6
n0
C7
n0
C8
n0
C9
n0
C10
n0
O0 0
o54
7
o2
o0
v12
n-1
o0
o2
n-1
v13
n1
o2
o0
v0
n-1
o0
o2
n-1
v6
n1
o2
o0
v1
n-1
o0
o2
n-1
v7
n1
o2
o0
v2
n-1
o0
o2
n-1
v8
n1
o2
o0
v3
n-1
o0
o2
n-1
v9
n1
o2
o0
v4
n-1
o0
o2
n-1
v10
n1
o2
o0
v5
n-1
o0
o2
n-1
v11
n1
x7
6 0
7 0
8 0
9 0
10 0
11 0
13 0
r
4 2.0
4 2.0
4 2.0
0 0.0 1.0
0 0.0 1.0
0 0.0 1.0
0 0.0 1.0
4 1.0
4 1.0
4 1.0
4 1.0
b
2 1
2 1
2 1
2 1
2 1
2 1
0 0 1
0 0 1
0 0 1
0 0 1
0 0 1
0 0 1
2 1
0 0 1
k13
2
4
6
8
10
12
15
18
20
22
24
26
27
J0 5
12 1
0 0
1 0
6 0
7 0
J1 5
0 1
2 0
3 0
8 0
9 0
J2 5
1 1
4 0
5 0
10 0
11 0
J3 3
13 1
6 1
8 1
J4 3
13 1
6 1
9 1
J5 3
13 1
7 1
10 1
J6 3
13 1
7 1
11 1
J7 1
2 1
J8 1
3 1
J9 1
4 1
J10 1
5 1
G0 14
0 0
1 0
2 0
3 0
4 0
5 0
6 0
7 0
8 0
9 0
10 0
11 0
12 0
13 0
